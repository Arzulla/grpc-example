package com.jabiyev.grpcdiscountservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GrpcDiscountServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrpcDiscountServiceApplication.class, args);
	}

}
