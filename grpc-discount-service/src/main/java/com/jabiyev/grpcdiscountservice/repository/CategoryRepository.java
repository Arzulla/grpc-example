package com.jabiyev.grpcdiscountservice.repository;

import com.jabiyev.grpcdiscountservice.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findByExternalId(Integer id);
}
