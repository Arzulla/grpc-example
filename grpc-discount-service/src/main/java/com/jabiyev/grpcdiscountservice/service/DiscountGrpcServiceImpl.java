package com.jabiyev.grpcdiscountservice.service;

import com.jabiyev.grpc.*;
import com.jabiyev.grpcdiscountservice.entity.Category;
import com.jabiyev.grpcdiscountservice.entity.Discount;
import com.jabiyev.grpcdiscountservice.repository.CategoryRepository;
import com.jabiyev.grpcdiscountservice.repository.DiscountRepository;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.server.service.GrpcService;

import java.math.BigDecimal;
import java.util.Optional;

@GrpcService
@RequiredArgsConstructor
public class DiscountGrpcServiceImpl extends DiscountServiceGrpc.DiscountServiceImplBase {
    private final DiscountRepository discountRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public void getDiscount(DiscountRequest request, StreamObserver<DiscountResponse> responseObserver) {

        System.out.println("Request"+ request.getCode()+" "+request.getPrice());


        Category category = categoryRepository.findByExternalId((int) request.getExternalCategoryId())
                .orElseThrow(() -> new RuntimeException("Category has not been found by external category ID"));

        Optional<Discount> discount = discountRepository.findByCodeAndCategoryId(request.getCode(), category.getId());

        if (discount.isPresent()) {
            BigDecimal newPrice=discount.get().getDiscountPrice()
                    .subtract(BigDecimal.valueOf(request.getPrice())).multiply(BigDecimal.valueOf(-1));

            responseObserver.onNext(DiscountResponse.newBuilder()
                    .setCode(discount.get().getCode())
                    .setOldPrice(request.getPrice())
                    .setNewPrice(newPrice.floatValue())
                    .setResponse(Response.newBuilder().setStatusCode(true)
                            .setMessage("Discount has applied successfully!")).build());
        } else {
            responseObserver.onNext(DiscountResponse.newBuilder().setCode(request.getCode()).setOldPrice(request.getPrice())
                    .setResponse(Response.newBuilder()
                            .setMessage("Code or category are invalid")
                            .setStatusCode(false).build()).build());
        }
        responseObserver.onCompleted();


        super.getDiscount(request, responseObserver);
    }
}
