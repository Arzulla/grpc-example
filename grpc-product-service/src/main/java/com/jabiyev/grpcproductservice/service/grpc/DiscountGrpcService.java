package com.jabiyev.grpcproductservice.service.grpc;

import com.jabiyev.grpc.DiscountRequest;
import com.jabiyev.grpc.DiscountResponse;

public interface DiscountGrpcService {
    DiscountResponse getDiscount(DiscountRequest discountRequest);
}
