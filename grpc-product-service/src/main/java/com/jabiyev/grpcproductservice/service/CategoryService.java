package com.jabiyev.grpcproductservice.service;

import com.jabiyev.grpcproductservice.entity.Category;
import com.jabiyev.grpcproductservice.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public Category add(Category category) {
        return categoryRepository.save(category);
    }
}
