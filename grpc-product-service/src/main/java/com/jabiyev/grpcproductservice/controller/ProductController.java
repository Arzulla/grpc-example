package com.jabiyev.grpcproductservice.controller;

import com.jabiyev.grpc.DiscountRequest;
import com.jabiyev.grpcproductservice.entity.Product;
import com.jabiyev.grpcproductservice.service.ProductService;
import com.jabiyev.grpcproductservice.service.grpc.DiscountGrpcService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("api/products")
@RequiredArgsConstructor
public class ProductController {
    private final DiscountGrpcService grpcService;
    private final ProductService productService;

    @PostMapping
    public ResponseEntity<Product> create(@RequestBody Product product) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productService.add(product));
    }


    @GetMapping
    public ResponseEntity<List<Product>> getAll() {
        grpcService.getDiscount(DiscountRequest.newBuilder()
                .setCode("Arzulla")
                .setPrice(BigDecimal.valueOf(20L).floatValue()).build());
        return ResponseEntity.status(HttpStatus.OK)
                .body(productService.getAll());
    }
}
