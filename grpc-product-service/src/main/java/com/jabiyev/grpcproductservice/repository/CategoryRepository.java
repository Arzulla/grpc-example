package com.jabiyev.grpcproductservice.repository;

import com.jabiyev.grpcproductservice.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
