package com.jabiyev.grpcproductservice.repository;

import com.jabiyev.grpcproductservice.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Integer> {
}
